### Pull image From Docker-Hub
There are 2 images you should pull From Docker-hub.

    Delete folder ./vue/node_modules
    docker-compose run --rm my-app npm install --no-save

### Run Docker

    1) docker-compose up
    2) connect DB:
    ("dev", user="thang", password="thang1999", host="172.17.0.1", port="7000")
 
### Create DB

    sudo docker-compose run --rm flask bash
    cd migrations
    python create_alls.py

### Run terminal
    docker-compose run --rm flask bash

### Done
