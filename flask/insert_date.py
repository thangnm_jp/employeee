from peewee import *
import openpyxl
from openpyxl.styles import colors, PatternFill, Font, Border
import string
from datetime import datetime, timedelta
import datetime
import calendar


def date(mq, yq):
    xfile = openpyxl.load_workbook('e.xlsx')
    sheet = xfile.get_sheet_by_name('1911')
    c = 5
    r = 13
    m = mq
    y = yq
    maxdate = calendar.monthrange(y, m)[1]
    x = datetime.date(y, m, 1)
    m = x.strftime("%B")
    y = x.strftime("%Y")
    a = m + " " + y
    sheet.cell(row=9, column=1).value = a
    weekends = 0
    for y in range(1, maxdate + 1):
        # print( x.strftime("%d")== 1)
        sheet.cell(row=r, column=c).value = x.strftime("%a")
        if (sheet.cell(row=r, column=c).value == 'Sun'):
            sheet.cell(row=r, column=c).fill = PatternFill(fill_type='solid', start_color='ff8327', end_color='ff8327')
            weekends += 1
        if (sheet.cell(row=r, column=c).value == 'Sat'):
            sheet.cell(row=r, column=c).fill = PatternFill(fill_type='solid', start_color='7ac5cd', end_color='7ac5cd')
            weekends += 1
        x = x + timedelta(days=1)
        c += 1
    days_expected = str(maxdate - weekends)

    print(datetime.datetime.now().strftime("%a"))
    xfile.save('e.xlsx')
    xfile.close()
    return int(days_expected)
