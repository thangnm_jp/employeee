from peewee import *
import openpyxl
import string
from datetime import datetime, timedelta
import datetime

con = PostgresqlDatabase("dev", user="thang", password="thang1999", host="172.17.0.1", port="7000")
import calendar
from openpyxl.comments import Comment
import insert_pay


class BaseModel(Model):
    class Meta:
        database = con


class Workoffs(BaseModel):
    e_id = IntegerField()
    dateoff = DateField()
    timeoff = IntegerField()
    reason = TextField()


class Employees(BaseModel):
    id = IntegerField()
    name = TextField()


def export_excel(mq, yq):
    m = mq
    y = yq
    maxdate = calendar.monthrange(y, m)[1]
    ws = Workoffs.select()
    result = []
    xfile = openpyxl.load_workbook('e.xlsx')
    sheet = xfile.get_sheet_by_name('1911')
    es = Employees.select()
    for w in ws:
        if int(w.dateoff.strftime('%m')) == m:
            e_id = w.e_id
            reason = w.reason
            timeoff = w.timeoff
            dateoff = w.dateoff.strftime("%d")
            r = 14
            for e in es:
                c = 5
                t = 0
                t_off = 0
                if int(sheet.cell(row=r + 1, column=1).value) == int(e_id):
                    # compare must be string ('not int')

                    for z in range(1, maxdate + 1):

                        if (sheet.cell(row=r, column=c).value == 1):
                            t += 1
                        if sheet.cell(row=12, column=c).value == int(dateoff):
                            comments = Comment(reason, 'Author')
                            sheet.cell(row=r + 1, column=c).comment = comments
                            # compare is the value it return(int)
                            if (timeoff == 8):
                                t_off = t_off + 1
                                sheet.cell(row=r, column=c).value = 0
                                sheet.cell(row=r + 1, column=c).value = "P1d"
                            elif (timeoff == 6):
                                t_off = t_off + 0.75
                                sheet.cell(row=r, column=c).value = 0.25
                                sheet.cell(row=r + 1, column=c).value = "P6h"
                            elif (timeoff == 4):
                                t_off = t_off + 0.5
                                sheet.cell(row=r, column=c).value = 0.5
                                sheet.cell(row=r + 1, column=c).value = "P4h"
                            elif (timeoff == 2):
                                t_off = t_off + 0.25
                                sheet.cell(row=r, column=c).value = 0.75
                                sheet.cell(row=r + 1, column=c).value = "P2h"
                        c += 1
                    sheet.cell(row=r + 1, column=36).value = t-t_off
                    sheet.cell(row=r + 1, column=38).value = t_off
                r += 3
    x = datetime.date(y, m, 1)
    m = x.strftime("%m")
    title = str(m) + str(y) + '.xlsx'
    xfile.save(title)
    xfile.close()
