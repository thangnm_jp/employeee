from peewee import *
from playhouse.shortcuts import model_to_dict, dict_to_model
import string
import datetime
import json

con = PostgresqlDatabase("dev", user="thang", password="thang1999", host="172.17.0.1", port="7000")

class BaseModel(Model):
    class Meta:
        database = con
class Salary(BaseModel):
    no = IntegerField()
    month = TextField()
    figure = TextField()
    pay = IntegerField()
    date = IntegerField()
    reward = IntegerField()
    total = IntegerField()
    note = TextField()

def money():
    sa = Salary.select()
    result = []
    for s in sa:
        result.append(model_to_dict(s))
    print(" Done")
    return json.dumps(result)


