from peewee import *
import openpyxl
from openpyxl.styles import colors, PatternFill, Font, Border, Side
import string
from datetime import datetime, timedelta
import datetime

con = PostgresqlDatabase("dev", user="thang", password="thang1999", host="172.17.0.1", port="7000")

thin_border = Border(left=Side(style='thin'),
                     right=Side(style='thin'),
                     top=Side(style='thin'),
                     bottom=Side(style='thin'))
border = Border(left=Side(style='thin'),
                right=Side(style='thin'))
bottom_border = Border(bottom=Side(style='thin'),
                       right=Side(style='thin'))


class BaseModel(Model):
    class Meta:
        database = con


class Employees(BaseModel):
    id = IntegerField()
    name = TextField()


def copyRange(startCol, startRow, endCol, endRow, sheet):
    rangeSelected = []
    # Loops through selected Rows
    for i in range(startRow, endRow + 1, 1):
        # Appends the row to a RowSelected list
        rowSelected = []
        for j in range(startCol, endCol + 1, 1):
            rowSelected.append(sheet.cell(row=i, column=j).value)
        rangeSelected.append(rowSelected)
    return rangeSelected


def pasteRange(startCol, startRow, endCol, endRow, sheetReceiving, copiedData):
    countRow = 0
    for i in range(startRow, endRow + 1, 1):
        countCol = 0
        for j in range(startCol, endCol + 1, 1):
            sheetReceiving.cell(row=i, column=j).value = copiedData[countRow][countCol]
            if (j == 3 or j == 36 or j == 37 or j == 38 or j == 39 or j == 40 or j == 2 or j == 1):
                sheetReceiving.cell(row=i, column=j).border = border
                if ((i - 1) % 3 == 0):
                    sheetReceiving.cell(row=i, column=j).border = bottom_border
            else:
                sheetReceiving.cell(row=i, column=j).border = thin_border
            countCol += 1
        countRow += 1


def createData():
    xfile = openpyxl.load_workbook("text1.xlsx")  # Add file name
    sheet = xfile.get_sheet_by_name("1911")  # Add Sheet name
    print("Processing...")
    es = Employees.select()
    selectedRange = copyRange(1, 14, 40, 16, sheet)
    r = 14
    c = 1
    e = es[0]
    id = e.id
    name = e.name
    sheet.cell(row=r + 1, column=c).value = id
    sheet.cell(row=r + 1, column=c + 1).value = name
    r += 3
    count = 0
    for e in es:
        count += 1
        if (count > 1):
            sheet.insert_rows(r, 3)
            pastingRange = pasteRange(1, r, 40, r + 2, sheet, selectedRange)
            id = e.id
            name = e.name
            sheet.cell(row=r + 1, column=c).value = id
            sheet.cell(row=r + 1, column=c + 1).value = name
            r += 3

    xfile.save("e.xlsx")
    xfile.close()
