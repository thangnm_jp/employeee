from models import index
from flask import request
import json
from playhouse.shortcuts import model_to_dict, dict_to_model


def get_employees():
    es = index.Employees.select()
    result = []
    for e in es:
        result.append(model_to_dict(e))
    return json.dumps(result)


def get_phep():
    es = index.Employees.select(index.Employees.phep)
    result = []
    for e in es:
        result.append(model_to_dict(e))
    return json.dumps(result)

def get_employee(e_id):
    es = index.Employees.select().where(index.Employees.id == e_id)
    result = []
    for e in es:
        result.append(model_to_dict(e))
    return json.dumps(result)
    # return json.dumps(es)


def delete_employee(e_id):
    index.Employees.delete().where(index.Employees.id == e_id).execute()
    # call function select employee
    select_employee()

def insert_employee():
    row = {
        'id': request.json['id'],
        'name': request.json['name'],
        'address': request.json['address'],
        'dob': request.json['dob'],
        'start': request.json['start'],
        'phone': request.json['phone'],
        'position': request.json['position'],
        'phep': request.json['phep']
    }
    index.Employees.insert(row).execute()
    return "success"

# edit
def update_employee(e_id):
    row = {
        'name': request.json['name'],
        'address': request.json['address'],
        'dob': request.json['dob'],
        'start': request.json['start'],
        'phone': request.json['phone'],
        'position': request.json['position']
    }

    index.Employees.update(row).where(index.Employees.id == e_id).execute()
    return "update success"



def search_employee():
    es = index.Employees.select().where(index.Employees.name == a)
    result = []
    for e in es:
        result.append(model_to_dict(e))
    return json.dumps(result)
