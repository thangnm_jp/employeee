from models import index
from flask import request
import json
from playhouse.shortcuts import model_to_dict, dict_to_model
import datetime


def get_salaries():
    es = index.Salaries.select()
    result = []
    for e in es:
        result.append(model_to_dict(e))
    return json.dumps(result, default=myconverter)


def insert_salaries():
    row = {
        'id': request.json['id'],
        'e_id': request.json['e_id'],
        'pay': request.json['pay'],
        'reward': request.json['reward'],
        'time': request.json['time'],
        'note': request.json['note'],
        'sub': request.json['sub'],
        'total': request.json['total']
    }
    index.Salaries.insert(row).execute()
    return "success"


def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()



def query():
    yq = request.json['year']
    mq = request.json['month']
    es = index.Salaries.select().where(index.Salaries.time.month == mq)
    result = []
    for e in es:
        result.append(model_to_dict(e))
    return json.dumps(result)
