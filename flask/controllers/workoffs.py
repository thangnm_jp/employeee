from models import index
from flask_cors import CORS
from peewee import *
from flask import request
import json
from playhouse.shortcuts import model_to_dict, dict_to_model
import calendar
from datetime import datetime, timedelta
import datetime
from flask import Flask

app = Flask(__name__)
CORS(app)
con = PostgresqlDatabase("dev", user="thang", password="thang1999", host="172.17.0.1", port="7000")


class BaseModel(Model):
    class Meta:
        database = con

def get_workoffs():
    ws = index.Workoffs.select()
    result = []
    for w in ws:
        result.append(model_to_dict(w))
    return json.dumps(result)


class Workoffs(BaseModel):
    e_id = IntegerField()
    dateoff = DateField()


def get_workoffs_by_month():
    current_m = datetime.datetime.now().strftime("%m")
    m = 6
    ws = Workoffs.select()
    result = []
    for w in ws:
        if int(w.dateoff.strftime('%m')) == m:
            result.append(model_to_dict(w))
    return json.dumps(result)

def delete_work(w_id):
    index.Workoffs.delete().where(index.Workoffs.id == w_id).execute()
    select_work()


def insert_work():
    if request.json['from'] == "":
        row = {
            'e_id': request.json['e_id'],
            'timeoff': request.json['timeoff'],
            'dateoff': request.json['to'],
            'reason': request.json['reason'],
        }
        index.Workoffs.insert(row).execute()
    elif request.json['to'] == "":
        row = {
            'e_id': request.json['e_id'],
            'timeoff': request.json['timeoff'],
            'dateoff': request.json['from'],
            'reason': request.json['reason'],
        }
        index.Workoffs.insert(row).execute()
    else:
        start = datetime.datetime.strptime(request.json['from'], "%Y-%m-%d")
        end = datetime.datetime.strptime(request.json['to'], "%Y-%m-%d")
        date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days + 1)]
        for i in date_generated:
            row = {
                'e_id': request.json['e_id'],
                'timeoff': request.json['timeoff'],
                'dateoff': i.strftime("%Y-%m-%d"),
                'reason': request.json['reason']
            }
            index.Workoffs.insert(row).execute()
    return "success insert workoff"
