from models import index
from flask import request, redirect
import json
from werkzeug.security import generate_password_hash
from playhouse.shortcuts import model_to_dict, dict_to_model


def register():
    password = request.json['password']
    confirm_password = request.json['confirm_password']
    if ( password == confirm_password):
        row = {
            'username': request.json['username'],
            'email': request.json['email'],
            'password': generate_password_hash(password, method='sha256')
        }
        index.Accounts.insert(row).execute()
        return "go in to login"
    else:
        return "register again"


def login():
    email = request.json['email']
    password = request.json['password']
    print(email, password)
    es = index.Accounts.select(index.Accounts.email).where(index.Accounts.email == email)
    print(es)
    # result = []
    # for e in es:
    #     result.append(model_to_dict(e))
    # return json.dumps(result)
    # return index.Accounts.select(index.Accounts.email).where(index.Accounts.email == email)
    return email
    # return redirect("http://127.0.0.1:8081", code=304)