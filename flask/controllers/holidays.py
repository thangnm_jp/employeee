from models import index
from flask import request
import json
import datetime
from playhouse.shortcuts import model_to_dict, dict_to_model


def select_holiday():
    holidays = index.Holidays.select()
    result = []
    for h in holidays:
        result.append(model_to_dict(h))
    return json.dumps(result)


def delete_holiday(h_id):
    index.Holidays.delete().where(index.Holidays.id == h_id).execute()
    select_holiday()


def insert_holiday():
    if request.json['from'] == "":
        row = {
            'name': request.json['name'],
            'dateoff': request.json['to'],
            'infor': request.json['infor'],
        }
        index.Holidays.insert(row).execute()
    elif request.json['to'] == "":
        row = {
            'name': request.json['name'],
            'dateoff': request.json['from'],
            'infor': request.json['infor'],
        }
        index.Holidays.insert(row).execute()

    else:
        start = datetime.datetime.strptime(request.json['from'], "%Y-%m-%d")
        end = datetime.datetime.strptime(request.json['to'], "%Y-%m-%d")
        date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days + 1)]
        for i in date_generated:
            row = {
                'name': request.json['name'],
                'dateoff': i.strftime("%Y-%m-%d"),
                'infor': request.json['infor']
            }
            index.Holidays.insert(row).execute()
    return "success holiday"
