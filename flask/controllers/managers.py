from models import index
from flask import request
import json
from playhouse.shortcuts import model_to_dict, dict_to_model

def select_manager():
    es = index.Employees.select()
    result = []
    for e in es:
        result.append(model_to_dict(e))
        print(result)
    return json.dumps(result)

def insert_manager():
    row = {
        'id': request.json['id'],
        'e_id': request.json['e_id'],
        'name': request.json['name'],
        'address': request.json['address'],
        'dob': request.json['dob'],
        'start': request.json['start'],
        'phone': request.json['phone'],
        'position': request.json['position']
    }
    index.Employees.insert(row).execute()
    return "success"
