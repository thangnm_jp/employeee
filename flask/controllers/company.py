from models import index
from flask import request
import json
from playhouse.shortcuts import model_to_dict, dict_to_model

def select_company():
    es = index.Company.select()
    result = []
    for e in es:
        result.append(model_to_dict(e))
        print(result)
    return json.dumps(result)

def insert_company():
    row = {
        'id': request.json['id'],
        'name': request.json['name'],
        'address': request.json['address'],
        'tax_code': request.json['tax_code'],
        'information': request.json['information'],
        'director': request.json['director'],
        'preparer': request.json['preparer'],
        'phone': request.json['phone']
    }
    index.Company.insert(row).execute()
    return "success"
