from peewee import *
import openpyxl
from openpyxl.styles import colors, PatternFill, Font, Border, Alignment
import string
import calendar
from datetime import datetime, timedelta
import datetime, insert_date

con = PostgresqlDatabase("dev", user="thang", password="thang1999", host="172.17.0.1", port="7000")


class BaseModel(Model):
    class Meta:
        database = con


class Employees(BaseModel):
    id = IntegerField()
    name = TextField()


class Holidays(BaseModel):
    dateoff = DateField()


def pay(mq, yq):
    m = mq
    y = yq
    maxdate = calendar.monthrange(y, m)[1]
    c = 5
    xfile = openpyxl.load_workbook('e.xlsx')
    sheet = xfile.get_sheet_by_name('1911')
    hs = Holidays.select()
    es = Employees.select()
    for y in range(1, maxdate + 1):
        r = 14
        for e in es:
            if (sheet.cell(row=13, column=c).value == "Sun" or sheet.cell(row=13, column=c).value == "Sat"):
                sheet.cell(row=r + 1, column=c).value = "U"
            else:
                sheet.cell(row=r, column=c).value = 1
            count = 0
            for h in hs:
                if int(h.dateoff.strftime("%m")) == m:
                    count = count + 1
                    if int(h.dateoff.strftime("%d")) == sheet.cell(row=12, column=c).value:

                        if sheet.cell(row=13, column=c).value == "Sat":
                            sheet.cell(row=r, column=c + 2).value = 0
                            sheet.cell(row=r + 1, column=c + 2).value = "H"
                        elif sheet.cell(row=13, column=c).value == "Sun":
                            sheet.cell(row=r, column=c + 1).value = 0
                            sheet.cell(row=r + 1, column=c + 1).value = "H"
                        else:
                            sheet.cell(row=r, column=c).value = 0
                            sheet.cell(row=r + 1, column=c).value = "H"
            r += 3
        c += 1

    c = 1
    for y in range(1, 40):
        r = 14
        for e in es:
            sheet.cell(row=r, column=c).alignment = Alignment(horizontal='center')
            sheet.cell(row=r + 1, column=c).alignment = Alignment(horizontal='center')
            sheet.cell(row=r + 2, column=c).alignment = Alignment(horizontal='center')
            r += 3
        c += 1

    r = 14
    for e in es:
        c = 5
        date = 0
        for z in range(1, maxdate + 1):
            if sheet.cell(row=r, column=c).value == 1:
                date += sheet.cell(row=r, column=c).value
            c += 1
        sheet.cell(row=r + 1, column=36).value = date
        r += 3
    sheet.cell(row=10, column=1).value = str(date) + " Days Expected"
    xfile.save('e.xlsx')
    xfile.close()
