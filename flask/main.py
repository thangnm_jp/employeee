# flask_web/main.py
from flask import Flask, render_template, jsonify, request, send_from_directory, abort, session, redirect
from flask_cors import CORS
import json
import insert_e, insert_date, insert_pay, export
from peewee import *
import datetime
from controllers import employees, workoffs, holidays, salaries, company, accounts
from models import index

app = Flask(__name__)
CORS(app)


@app.route("/")
def helloWorld():
    return "Hello, cross-origin-world!"


@app.route('/about/<xlsx_name>')
def about(xlsx_name):
    return send_from_directory(link, xlsx_name, as_attachment=True)


@app.route('/accounts/register', methods=['POST'])
def register_account():
    return accounts.register()


@app.route('/accounts/login', methods=['POST', "GET"])
def login_post():
    if request.method == "POST":
        return accounts.login()
    else:
        return "heelo"

@app.route('/user')
def user():
    if "user" in session:
        user = session["user"]
        return f"<h1>{user}</h1>"
    else:
        return redirect(url_for("login"))

@app.route('/logout')
def logout():
    session.pop("user", None)
    return redirect(url_for("login"))


@app.route('/workoffs', methods=['GET'])
def selectWork():
    return workoffs.get_workoffs()


@app.route('/workoffsbymonth', methods=['GET'])
def selectWorkByMonth():
    return workoffs.get_workoffs_by_month()


@app.route('/workoffs', methods=['POST'])
def insertWork():
    return workoffs.insert_work()

@app.route('/workoffs/<string:id>', methods=['DELETE'])
def deleteWorkoff(id):
    return workoffs.delete_work(id)


@app.route('/employees', methods=['GET'])
def selectEmployee():
    return employees.get_employees()


@app.route('/employees/phep', methods=['GET'])
def selectPhepEmployee():
    return employees.get_phep()


@app.route('/employees/<string:id>', methods=['GET'])
def getEmployee(id):
    return employees.get_employee(id)



@app.route('/employees', methods=['POST'])
def insertEmployee():
    return employees.insert_employee()

@app.route('/employees/edit/<string:id>/', methods=['PUT'])
def updateEmployee(id):
    return employees.update_employee(id)


@app.route('/employees/<string:id>', methods=['DELETE'])
def deleteEmployee(id):
    return employees.delete_employee(id)


@app.route('/holidays', methods=['GET'])
def selectHoliday():
    return holidays.select_holiday()


@app.route('/holidays', methods=['POST'])
def insertHoliday():
    return holidays.insert_holiday()


@app.route('/holidays/<string:id>', methods=['DELETE'])
def deleteHoliday(id):
    return holidays.delete_holiday(id)


@app.route('/salaries', methods=['GET'])
def selectSalaries():
    return salaries.get_salaries()


@app.route('/salaries', methods=['POST'])
def query_salary():
    return salaries.query()


@app.route('/company', methods=['GET'])
def selectCompany():
    return company.select_company()


@app.route('/company', methods=['POST'])
def insertCompany():
    return company.insert_company()



@app.route('/exportfile', methods=['POST'])
def query1():
    yq = request.json['year']
    mq = request.json['month']
    print(yq, mq)
    insert_e.createData()
    insert_date.date(int(mq), int(yq))
    insert_pay.pay(int(mq), int(yq))
    export.export_excel(int(mq), int(yq))
    return mq
