def up(db):
    db.execute_sql("""
                CREATE TABLE managers (
                  id BIGSERIAL PRIMARY KEY,
                  e_id BIGSERIAL FOREIGN KEY,
                  name TEXT,
                  address TEXT,
                  dob DATE,
                  star DATE,
                  end DATE,
                  posision TEXT,
                  created_at TIMESTAMPTZ DEFAULT NOW()
                )
            """)