def up(db):
    db.execute_sql("""
                CREATE TABLE workoffs (
                  id BIGSERIAL PRIMARY KEY,
                  e_id BIGSERIAL FOREIGN KEY,
                  name TEXT,
                  timeoff BIGSERIAL
                  dateoff DATE,
                  reason TEXT,
                  created_at TIMESTAMPTZ DEFAULT NOW()
                )
            """)