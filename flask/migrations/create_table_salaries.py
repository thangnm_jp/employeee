def up(db):
    db.execute_sql("""
                CREATE TABLE salaries (
                  id BIGSERIAL PRIMARY KEY,
                  e_id BIGSERIAL FOREIGN KEY,
                  pay BIGSERIAL,
                  figure DOUBLE,
                  reward BIGSERIAL,
                  time DATE,
                  sub BIGSERIAL,
                  total BIGSERIAL,
                  created_at TIMESTAMPTZ DEFAULT NOW()
                )
            """)