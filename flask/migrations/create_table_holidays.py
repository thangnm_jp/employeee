def up(db):
    db.execute_sql("""
                CREATE TABLE holidays (
                  id BIGSERIAL PRIMARY KEY,
                  name TEXT,
                  dateoff DATE,
                  infor TEXT,
                  created_at TIMESTAMPTZ DEFAULT NOW()
                )
            """)