from peewee import *
import create_table_employees

con = PostgresqlDatabase("dev", user="thang", password="thang1999", host="172.17.0.1", port="7000")
cur = con.cursor()
print("connect successfully")


def create_table_employees():
    cur.execute(
        """
            CREATE TABLE Employees(
                ID INT PRIMARY KEY NOT NULL,
                NAME TEXT,
                DOB DATE,
                START DATE,
                ADDRESS TEXT,
                PHONE INT,
                POSITION TEXT,
                PHEP INT
            )
        """)
    con.commit()
    print("create table employees successfully")


def create_table_accounts():
    cur.execute(
        """
            CREATE TABLE Accounts(
                ID INT PRIMARY KEY NOT NULL,
                USERNAME TEXT,
                PASSWORD TEXT,
                AVATA TEXT
            )
        """)
    con.commit()
    print("create table accounts successfully")


def create_table_managers():
    cur.execute(
        """
            CREATE TABLE Managers(
                ID INT PRIMARY KEY NOT NULL,
                E_ID INT,
                NAME TEXT,
                DOB DATE,
                START DATE,
                ADDRESS TEXT,
                PHONE INT,
                POSITION TEXT
            )
        """)
    con.commit()
    print("create table managers successfully")


def create_table_workoffs():
    cur.execute(
        """
            CREATE TABLE Workoffs(
                ID INT PRIMARY KEY NOT NULL,
                E_ID INT,
                TIMEOFF INT,
                DATEOFF DATE,
                REASON TEXT
            )
        """)
    con.commit()
    print("create table workoffs successfully")


def create_table_holidays():
    cur.execute(
        """
            CREATE TABLE Holidays(
                ID INT PRIMARY KEY NOT NULL,
                NAME TEXT,
                DATEOFF DATE,
                INFOR TEXT
            )
        """)
    con.commit()
    print("create table holidays successfully")


def create_table_salaries():
    cur.execute(
        """
            CREATE TABLE Salaries(
                ID INT PRIMARY KEY NOT NULL,
                E_ID INT,
                PAY INT,
                FIGURE FLOAT ,
                REWARD INT,
                TIME DATE,
                NOTE TEXT,
                SUB INT,
                TOTAL INT
            )
        """)
    con.commit()
    print("create table salaries successfully")


def create_table_company():
    cur.execute(
        """
            CREATE TABLE Company(
                ID INT PRIMARY KEY NOT NULL,
                NAME TEXT,
                ADDRESS TEXT,
                TAXCODE TEXT
            )
        """)
    con.commit()
    print("create table company successfully")


create_table_employees()
create_table_accounts()
create_table_managers()
create_table_workoffs()
create_table_holidays()
create_table_salaries()
create_table_company()
