from peewee import *
import fillform
import openpyxl
import string


con = PostgresqlDatabase("dev", user="thang", password="thang1999", host="172.17.0.1", port="7000")


class BaseModel(Model):
    class Meta:
        database = con


class Accounts(BaseModel):
    username = TextField()
    email = TextField()
    password = TextField()


class Employees(BaseModel):
    id = IntegerField()
    name = TextField()
    address = TextField()
    dob = TextField()
    start = TextField()
    phone = IntegerField()
    position = TextField()
    phep = IntegerField()


class Workoffs(BaseModel):
    id = IntegerField()
    e_id = IntegerField()
    timeoff = IntegerField()
    dateoff = TextField()
    reason = TextField()


class Holidays(BaseModel):
    id = IntegerField()
    name = TextField()
    dateoff = TextField()
    infor = TextField()


class Salaries(BaseModel):
    id = IntegerField()
    e_id = IntegerField()
    pay = IntegerField()
    reward = IntegerField()
    figure = DoubleField()
    time = TextField()
    note = TextField()
    sub = IntegerField()
    total = IntegerField()


class Company(BaseModel):
    id = IntegerField()
    name = TextField()
    address = TextField()
    tax_code = TextField()
    information = TextField()
    director = TextField()
    preparer = TextField()
    phone = IntegerField()