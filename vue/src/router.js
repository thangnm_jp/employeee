import Vue from 'vue';
import Router from 'vue-router';

import Addemployee from "./components/Addemloyee";
import Addworkoff from "./components/Addworkoff";
import Addholiday from "./components/Addholiday";
import Addsalary from "./components/Addsalary";
import Login from "./components/Login";
import Employees from './components/Employees.vue';
import Workoff from "./components/Workoff";
import Holiday from "./components/Holiday";
import Home from "./components/Home";
import Salary from "./components/Salary";
import Dashboard from "./components/Dashboard";
import Register from "./components/Register";
import Export from "./components/Export1";
import Editemployee from "./components/Editemployee";
Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path:'/dashboard',
            name :'Dashboard',
            component:Dashboard
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
        },
        {
            path: '/register',
            name: 'Register',
            component: Register,
        },
        {
            path: '/',
            name: 'Home',
            component: Home,
        },
        {
            path: '/employees',
            name: 'Employees',
            component: Employees,
        },
        {
            path: '/employees/:id/edit',
            name: 'edit employee',
            component: Editemployee,
            params : true
        },
        {
            path:'/addworkoff',
            name:'Addworkoff',
            component: Addworkoff,
        },
        {
            path:'/workoffs',
            name:'Workoff',
            component: Workoff,
        },
        {
            path: '/addemployee',
            name: 'Addemployee',
            component: Addemployee,
        },
        {
            path:'/export',
            name:'Export',
            component: Export,
        },

        {
            path:"/addholiday",
            name:"AddHoliday",
            component:Addholiday,
        },
        {
            path:"/holidays",
            name:"Holiday",
            component:Holiday,
        },
        {
            path:"/addsalary",
            name:"Addsalary",
            component:Addsalary,
        },
        {
            path:"/salary",
            name:"Salary",
            component:Salary,
        }
    ],
});